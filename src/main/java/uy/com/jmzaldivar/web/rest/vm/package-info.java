/**
 * View Models used by Spring MVC REST controllers.
 */
package uy.com.jmzaldivar.web.rest.vm;
